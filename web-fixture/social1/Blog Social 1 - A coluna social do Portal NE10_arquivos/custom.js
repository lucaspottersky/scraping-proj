var expandeBusca = false;
$(function () {
    $('ul').each(function () {
        $('li:last', $(this)).addClass('last');
        $('li:first', $(this)).addClass('first');
    });

	
	$("#btnSearch").click(function(){
		$("#searchform").submit();
	});

    // Inicialização do formulário de busca
    $('#searchBlog').click(function () {
        if (expandeBusca == false) {


            $('.inputtexto').animate({'width': 0}, 0).show();
            $('.inputtexto').animate({'width': '350px'}, 500);
            expandeBusca = true;
            $("#inputBusca").focus();
            return false;
        }
        else {
            expandeBusca = false;
        }

    });
    $('.inputtexto').mouseout(function () {
        if (!$("#inputBusca").val()) {
            $('.inputtexto').animate({'width': '50px'}, 500, function () {
                $(this).hide();
            });
            expandeBusca = false;
        }
    });

    // Estilizando box de ofertas da sidebar

    $('.oferta').each(function () {
        $('img', $(this)).addClass('img-responsive');
    });
    //$(this).append($('<div class="clearfix"></div>'));

    $('.oferta:odd').each(function () {
        $(this).after("<div class='clearfix'>");
    });

    $('.reply').each(function () {
        $('a', $(this)).addClass('botao-ver-mais');
    });

    var windowsize = $(window).width();
    $(window).resize(function () {
        var windowsize = $(window).width();
    });
    if (windowsize < 998) {
        $('.embed-audio').addClass('hidden');
    } else if (windowsize > 998) {
    }


    // Disqus
    $('#dsq-2').addClass('embed-responsive-item');
    $('#disqus_thread').css("width", "100%");
});

/* funcao player uol */
function playerVideoUol(idVideoAnalisado, divPlayer) {
    var nav = navigator, navP = nav.plugins, navM = nav.mimeTypes, flashT = 'application/x-shockwave-flash', hasFlash = nav && navP && navP.length > 0 && navM && navM[flashT] && navM[flashT].enabledPlugin && true || false;
    var html;
    var userAgent = navigator.userAgent;
    (userAgent.indexOf("MSIE") > -1) ? (browserVersion = "IE") : (browserVersion = "NOT IE");
    if (browserVersion == "IE" || hasFlash) {
        html = '<object width="730" height="410" id="player_' + idVideoAnalisado + '" >';
        html += '<param value="true" name="allowfullscreen"/>';
        html += '<param value="http://storage.mais.uol.com.br/embed_v2.swf?mediaId=' + idVideoAnalisado + ' name="movie"/>';
        html += '<param value="always" name="allowscriptaccess"/>';
        html += '<param value="window" name="wmode"/>';
        html += '<embed id="player_' + idVideoAnalisado + '" width="730" height="410" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" src="http://storage.mais.uol.com.br/embed_v2.swf?mediaId=' + idVideoAnalisado + '" wmode="window" /></embed>';
        html += '</object>';
        document.getElementById("player_" + divPlayer).innerHTML = html;
    } else {
        //html = '<video poster="http://thumb.mais.uol.com.br/' + idVideoAnalisado + '-medium.jpg?ver=' + thumbVersion + '" src="http://storage.mais.uol.com.br/' + idVideoAnalisado + '.mp4?ver=' + thumbVersion + '" type="video/mp4" width="730" height="410" controls autoplay autobuffer />';
		html = '<iframe src="http://mais.uol.com.br/static/uolplayer/?mediaId=' + idVideoAnalisado + '" width="730" height="410" allowfullscreen frameborder="0"></iframe>';
        document.getElementById("player_" + divPlayer).innerHTML = html;
    }
}