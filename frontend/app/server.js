'use strict';
const Promise = require('promise');
const exphbs  = require('express-handlebars');
const express = require('express');
const sqlite3 = require('sqlite3');
const fs = require('fs'); // TODO lna. move to inner scope
const morgan = require('morgan');
const requestify = require('requestify');
const PORT = 7654;
const SCRAPPY_LISTJOBS = 'http://scrappy:6800/listjobs.json?project=scrashtest';
const SCRAPPY_LISTSPIDERS = 'http://scrappy:6800/listspiders.json?project=scrashtest';

// Database setup
var setupDb = function() {
  var db = new sqlite3.Database('/sqlite-data/database.db', sqlite3.OPEN_READWRITE, result => {
    console.log("opened db?", result == null);
  });

  return db;
};
var db = setupDb();



// === UTILS ===
var appErrorHandler = function(err) {
  console.error('error!');
  console.error(err);
  throw err;
};

var allKeywords = function () {
  var promise = new Promise(function (resolve, reject) {
    try {
      var queryAll = db.all("SELECT * FROM keywords", function(err, rows) {
          resolve(rows);
      });      
    } catch (err) {
      reject(err);
    }
  });

  return promise;
};

var availableSpiders = function () {
  var promise = new Promise(function (resolve, reject) {
    try {
      requestify.get(SCRAPPY_LISTSPIDERS).then(resp => {
        resolve(JSON.parse(resp.body).spiders);
      });
    } catch (err) {
      reject(err);
    }
  });

  return promise;
};

var keywordsJoin = function(arr) {
  return arr.map(function (k) { return k.keyword; }).join(';');
};


// === APP SETUP ===
const app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
server.listen(PORT, function(){
  console.log('listening on *:' + PORT);
});

// app.listen(PORT);
app.use(express.static('public'));

// Register '.handlebars' extension with The Mustache Express
app.engine('handlebars', exphbs({
    defaultLayout: 'main',
    helpers: {
      encodeURIComponent: function(str) {
        return encodeURIComponent(str);
      },
      humanize: function(str) {
        if (typeof str !== 'string') {
          console.error('invalid type error. expected string.', str);
          return str;
        }              
        str = str.toLowerCase().replace(/[_-]+/g, ' ').replace(/\s{2,}/g, ' ').trim();
        str = str.charAt(0).toUpperCase() + str.slice(1);
      
        return str;
      }
    }
}));
app.set('view engine', 'handlebars');
app.set('views', './views');


// POST data middleware
var bodyParser = require('body-parser');
app.use(bodyParser.json());         // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

app.use(morgan("default"));

// Middleware to fetch common server data
app.use((req, res, next) => {
  var promises = [allKeywords(), availableSpiders()];
    
  Promise.all(promises).then(results => {
      res.locals.keywords = results[0];
      res.locals.availableSpiders = results[1];
      
      next();
    }).catch(appErrorHandler);
});

// === WEB SOCKETS ===
io.on('connection', function(socket){
  console.log('===> Connected.', socket.id);

  socket.on("disconnect", function() {
    console.log("===> Socket " + socket.id +" disconnected.");
  });
});

var pushEvents = function() {
  requestify.get(SCRAPPY_LISTJOBS).then(jobsResponse => {
    io.emit('server:spidersJobs', jobsResponse);
  });
};
setInterval(pushEvents.bind(app), 10000); // TODO lna. only if there's at least 1 websocket connected



// === CONTROLLERS ===
app.get('/', function (req, res) {
  res.render('index');
});

app.get('/spiders.json', function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  requestify.get(SCRAPPY_LISTSPIDERS).then(spiders => {
    res.send(JSON.stringify(spiders));  
  });
});

app.get('/spiders/:spider_name', function (req, res) {
  var s = req.params.spider_name;
  res.render('spiders/index', { spider_name: s, filename: '/scraped_data/'+s+'.json'});
});

app.get('/keyword/new', function (req, res) {
  res.render('keywords/new');
});

app.post('/keyword/new', function (req, res) {
  var kw = req.body.keyword;

  db.run("INSERT INTO keywords VALUES (?)", [kw], result => {    
    allKeywords().then(arr => {
      res.locals.keywords = arr;
      res.render('index', {message: 'Salvo com sucesso'});
    });
  });
});

app.get('/keywords/delete/:keyword', function (req, res) {
  db.run("DELETE FROM keywords WHERE keyword = ?", [req.params.keyword], result => {
    allKeywords().then(arr => {
      res.locals.keywords = arr;
      res.render('index', {message: 'Apagado com sucesso'});
    });    
  });      
});

app.get('/keywords/:keyword', function (req, res) {
  var s = encodeURIComponent(req.params.keyword);
  res.render('spiders/index', { filename: '/keywords/'+ s + '/json'});
});

app.get('/keywords/:keyword/json', function (req, res) {
  var fs = require('fs');
  var files = fs.readdirSync('/scraped_data').filter(function(filename, i) {
    return filename.endsWith('.json');
  });
  var contents = [];
  files.forEach(function(filename, i){
    var str = fs.readFileSync('/scraped_data/' + filename, 'utf8');
    try {
      contents = contents.concat(JSON.parse(str));
    } catch (e) {
      console.log('File contains invalid JSON. Empty?', filename);
    }
    
  });

  var kw = req.params.keyword;
  console.log("keyword", kw);
  contents = contents.filter(function(elem) {
    return (elem.keyword == kw);
  });

  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(contents));
});



var isSpiderScheduled = function(spider) {
  var p = new Promise(function (resolve, reject) {
    requestify.get(SCRAPPY_LISTJOBS).then(function(jobsResp) {
      var alreadyRunning = jobsResp.getBody().running.concat(jobsResp.getBody().pending).some((val, i, arr) => {
        console.log('val', val);
        return val.spider == spider;
      });

      resolve(alreadyRunning);
    });
  });

  return p;
};

var runSpider = function(spider, keywordsStr) {
  var promise = new Promise(function (resolve, reject) {
    var outputFilename = spider + '.json'; // example: social1.json

    isSpiderScheduled(spider).then((isScheduled) => {
        if (isScheduled) {
          resolve({status: 'ok', msg: 'already scheduled'});
          return;
        }

        requestify.request(
            'http://scrappy:6800/schedule.json',
            {
              method: 'POST',
              dataType: 'form-url-encoded',
              body: {
                setting: [
                  'DEPTH_LIMIT=2', 
                  'DOWNLOAD_DELAY=0.1', 
                  'FEED_URI=file:///app/crawler/output/'+ outputFilename
                ],
                project: 'scrashtest',
                spider: spider,
                keywords: keywordsStr // separated by semicolons
              },
            }
        ).then(function (response) {
        
          // Scrapyd will try to append to file, so we reset it before it tries to.
          if (response.getBody().status == 'ok') {
            var fse = require('fs-extra');
            fse.emptyDirSync('/scraped_data/');
            //fs.truncateSync('/scraped_data/'+outputFilename, 0);
          }          
          resolve(response);
        }).catch(function(response) {
          console.log('request failed. scrapyd fora do ar?', response);
          reject(response);
        });  
      });      
    });


  return promise;
};

app.post('/spiders/schedule', function (req, res) {
  // [{keyword: 'abc'}, ...] => 'abc;def;...'
  var keywords = res.locals.keywords;
  var keywordsStr = keywordsJoin(keywords);
  
  var promises = [];
  var allSuccess = false;
  var addToArr = function(resp) {
    resp = JSON.parse(resp.body);
    resp.spiders.forEach(function(s) {
      promises.push(runSpider(s, keywordsStr));
    });
  };
  requestify.get(SCRAPPY_LISTSPIDERS).then(addToArr).catch(appErrorHandler);
  
  Promise.all(promises).then(function(results) {
    allSuccess = results.every(function(r){
      return (r.status == 'ok' || (r.getBody && r.getBody().status == 'ok'));
    });

    var msg = allSuccess ? 'Atualizando...':'Falhou!!!  '+JSON.stringify(results);
    res.render('index', { message: msg, keywords: keywords });
  }).catch(appErrorHandler);
});


console.log('Running on http://localhost:' + PORT);
