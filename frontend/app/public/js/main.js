wrapImage = function(val) {
  return "<div class='crop'><a target='_blank' href='#v#'><img src='#v#' /></a></div>".replace(/#v#/g, "/scraped_data/prints/"+val);
};

$(document).ready(function() {
  var u = new URL(window.location);
  var socket = io('http://'+u.host, {reconnection: false});

  socket.on('connect', function () {
    console.debug("connect...");

    socket.on('server:spidersJobs', function(data){
      console.log(data);
      var jobs = JSON.parse(data.body);
      jobs = jobs.running.concat(jobs.pending);

      $.get('/spiders.json', function(resp) {
        var availableSpiders = JSON.parse(resp.body).spiders;

        availableSpiders.forEach(function(spider){
          var templ = '#spiders-menu i[id="#SPIDER_NAME#_spinner"]';
          var $spinner = $(templ.replace("#SPIDER_NAME#", spider));
          var isRunning = (jobs.filter(function(e){ return e.spider == spider; }).length) > 0;
          if (isRunning) {
            $spinner.css('visibility', 'visible');
          } else {
            $spinner.css('visibility', 'hidden');
          }
        });
      });
    });

    socket.on('disconnect', function(data){
      console.log('lost connection');
    });
  });
});
