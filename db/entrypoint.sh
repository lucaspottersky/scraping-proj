#!/bin/bash
set -e

cat /schema.sql | sqlite3 /sqlite-data/database.db

exec "$@"
