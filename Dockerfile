FROM python:2.7

RUN pip install scrapy scrapy-splash scrapyd scrapyd-client

RUN mkdir /app

VOLUME /scrapyd-data

WORKDIR /scrapyd-data

RUN mkdir -p /etc/scrapyd/
RUN echo "[scrapyd]" > /etc/scrapyd/scrapyd.conf
RUN echo "items_dir=/app/crawler/output" >> /etc/scrapyd/scrapyd.conf

CMD rm -f /scrapyd-data/twistd.pid && scrapyd