# -*- coding: utf-8 -*-
from w3lib.html import remove_tags

from scrapy.spiders import Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.exceptions import CloseSpider

from scrashtest.items import Page
from scrashtest.util import EMPTY_DATE, write_file_basename, gen_filepath
from scrashtest.spiders.clipina_base import ClipinaBaseSpider


class LocalSpider(ClipinaBaseSpider):
    name = "local"
    allowed_domains = ["web-fixture"]
    
    start_urls = [
        "http://web-fixture/simple"
    ]
    rules = [
        Rule(LinkExtractor(),
            follow=True, 
            process_request="prepare_splash_request"
            )
    ]
    
    def extract_item(self, response, keyword, match):
        self.logger.debug("## extract_item ## %s ", keyword)

        item = Page()
        item['url'] = response.url
        item['keyword'] = keyword
        item['published_at'] = EMPTY_DATE
        item['excerpt'] = remove_tags(match.extract())
        item['screenshot_low'] = write_file_basename(gen_filepath(response.url, 'jpeg'), response.data['jpeg'])

        return item
