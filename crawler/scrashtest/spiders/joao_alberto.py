import re
import datetime
from w3lib.html import remove_tags

from scrapy.spiders import Spider, CrawlSpider, Rule
from scrapy.spiders import Rule
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor

from scrashtest.items import Page
from scrashtest.util import *
from scrashtest.spiders.clipina_base import ClipinaBaseSpider


def try_extract_date(logger, url, current_selection):
    if try_extract_date_regex(logger, url):
        return try_extract_date_regex(logger, url)

    if try_extract_date_article(logger, current_selection):
        return try_extract_date_article(logger, current_selection)

    if try_extract_date_home(logger, current_selection):
        return try_extract_date_home(logger, current_selection)

    return EMPTY_DATE

def try_extract_date_article(logger, current_selection):
    post_meta = current_selection.xpath("ancestor::article//p[@class='post-meta']").extract_first()
    return try_extract_date_regex(logger, post_meta)

    
def try_extract_date_home(logger, current_selection):
    post_meta = current_selection.xpath("ancestor::div[contains(@class, 'recent-post')]//p[@class='post-meta']").extract_first()
    return try_extract_date_regex(logger, post_meta)

def try_extract_date_regex(logger, str):
    logger.debug('### try_extract_date_regex ### %s' % str)

    if str is None:
        return None

    m1 = REGEX_DDMMYYYY.search(str)
    if m1 is not None:
        return tryFmtDateOrNone(m1.group(0), '%d/%m/%Y')

    m2 = REGEX_YYYYMMDD.search(str)
    if m2 is not None:
        return tryFmtDateOrNone(m2.group(0), '%Y/%m/%d')

    return None


class JoaoAlbertoSpider(ClipinaBaseSpider):
    name = "joao_alberto"
    allowed_domains = ["joaoalberto.com"]
    start_urls = [
        "http://www.joaoalberto.com/"
    ]

    def extract_item(self, response, keyword, match):
        self.logger.debug("## extract_item ## %s ", keyword)

        item = Page()
        item['url'] = response.url
        item['keyword'] = keyword
        item['published_at'] = try_extract_date(self.logger, response.url, match)
        item['excerpt'] = remove_tags(match.extract())
        item['screenshot_low'] = write_file_basename(gen_filepath(response.url, 'jpeg'), response.data['jpeg'])

        return item
