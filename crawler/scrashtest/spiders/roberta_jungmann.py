import re
import datetime
from w3lib.html import remove_tags

from scrapy.spiders import Spider, CrawlSpider, Rule
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from scrapy.exceptions import CloseSpider

from scrashtest.items import Page
from scrashtest.util import *
from scrashtest.spiders.clipina_base import ClipinaBaseSpider


def try_extract_date_article(logger, current_selection):
    post_meta = current_selection.xpath("ancestor::div[@class= 'post']/span[@class='author']").extract_first()
    logger.debug('### POST_META ### %s' % post_meta)
    
    m = REGEX_DDMMYYYY.search(post_meta) if post_meta is not None else None
    date = EMPTY_DATE2 if m is None else m.group(0)
    date = datetime.datetime.strptime(date, '%d/%m/%Y').strftime('%Y/%m/%d')
    return date


class RobertaJungmannSpider(ClipinaBaseSpider):
    name = "roberta_jungmann"
    keywords = []
    allowed_domains = ["robertajungmann.com.br"]
    start_urls = [
        "http://www.robertajungmann.com.br"
    ]
    rules = [
        Rule(LinkExtractor(deny = (
                re.escape('http://www.robertajungmann.com.br/posts/maislidos') + '.*',
                re.escape('http://www.robertajungmann.com.br/posts/tag/') + '.*'
            )),
            follow = True,
            process_request = "prepare_splash_request"
        )
    ]

    def extract_item(self, response, keyword, match):
        self.logger.debug("## PARSE REQUEST ## %s")

        item = Page()
        item['url'] = response.url
        item['keyword'] = keyword
        item['published_at'] = try_extract_date_article(self.logger, match)
        item['excerpt'] = remove_tags(match.extract())
        item['screenshot_low'] = write_file_basename(gen_filepath(response.url, 'jpeg'), response.data['jpeg'])

        return item