# -*- coding: utf-8 -*-

from scrapy.spiders import Spider, CrawlSpider, Rule
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from scrapy.exceptions import CloseSpider

from scrashtest.util import *


class ClipinaBaseSpider(CrawlSpider):
    keywords = []
    rules = [
        Rule(LinkExtractor(),
            follow=True,
            process_request="prepare_splash_request"
            )
    ]

    def __init__(self, keywords='', *args, **kwargs):
        super(ClipinaBaseSpider, self).__init__(*args, **kwargs)

        if not self.start_urls or not self.allowed_domains:
            raise CloseSpider("Spider did not implement: start_urls or allowed_domains")

        if not keywords:
            raise CloseSpider("keywords array is empty. forgot to set argument?")

        keywords = unicode(keywords, 'utf-8')
        if keywords != '':
            self.keywords = keywords.split(';')

        self.logger.info('## KEYWORDS ##: %s', self.keywords)

    def parse_start_url(self, response):
        self.logger.debug("## parse_start_url ## ")

        # TODO lna. Calling start url 2x (1st time by Scrapy, 2nd by Splash)
        return splash_factory(response.url, self.parse_request, self.keywords)

    def prepare_splash_request(self, request):
        self.logger.debug("## prepare_splash_request ## req = %s" % (request))

        return splash_factory(request.url, self.parse_request, self.keywords)

    def extract_item(self, response, keyword, match):
        raise NotImplementedError

    def parse_request(self, response):
        self.logger.debug("## PARSE REQUEST ## %s")

        dir(response.data)
        self.logger.debug("console.. %s", response.data['console'])

        items = []
        sel = Selector(response)
        for keyword in self.keywords:
            xpath = "//*[text()[contains(translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'%s')]]" % keyword.lower()
            matches = sel.xpath(xpath)
            if len(matches) > 0:
                self.logger.info('## MATCHES ##: %s %s %s', keyword, len(matches), response.url)

            for match in matches:
                # Callback with response, keyword, match
                items.append(self.extract_item(response, keyword, match))

        return items
