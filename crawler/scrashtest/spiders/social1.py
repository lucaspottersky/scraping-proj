import re
from w3lib.html import remove_tags

from scrapy.spiders import Spider, CrawlSpider, Rule
from scrapy.selector import Selector
from scrapy.linkextractors import LinkExtractor
from scrapy.exceptions import CloseSpider

from scrashtest.items import Page
from scrashtest.util import *
from scrashtest.spiders.clipina_base import ClipinaBaseSpider

class Social1Spider(ClipinaBaseSpider):
    name = "social1"
    keywords = []
    allowed_domains = ["blogs.ne10.uol.com.br"]
    start_urls = [
        "http://blogs.ne10.uol.com.br/social1/"
    ]
    rules = [
        Rule(LinkExtractor(allow=re.escape('http://blogs.ne10.uol.com.br/social1/')+'.*'), follow=True, process_request="prepare_splash_request")
    ]

    def extract_item(self, response, keyword, match):
        self.logger.debug("## PARSE REQUEST ## %s")
        
        item = Page()
        item['url'] = response.url
        item['keyword'] = keyword
        m = REGEX_YYYYMMDD.search(response.url)
        item['published_at'] = EMPTY_DATE if m is None else m.group(0)
        item['excerpt'] = remove_tags(match.extract())
        item['screenshot_low'] = write_file_basename(gen_filepath(response.url, 'jpeg'), response.data['jpeg'])

        return item
