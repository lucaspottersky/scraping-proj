# -*- coding: utf-8 -*-

BOT_NAME = 'scrashtest'

SPIDER_MODULES = ['scrashtest.spiders']
NEWSPIDER_MODULE = 'scrashtest.spiders'

DOWNLOADER_MIDDLEWARES = {
    # Engine side
    'scrapy_splash.SplashCookiesMiddleware': 723,
    'scrapy_splash.SplashMiddleware': 725,
    'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,
    # Downloader side
}

SPIDER_MIDDLEWARES = {
    'scrapy_splash.SplashDeduplicateArgsMiddleware': 100,
}
#SPLASH_URL = 'http://127.0.0.1:8050/'
SPLASH_URL = 'http://splash:8050/'
DUPEFILTER_CLASS = 'scrapy_splash.SplashAwareDupeFilter'
HTTPCACHE_STORAGE = 'scrapy_splash.SplashAwareFSCacheStorage'

DEPTH_LIMIT=1
DOWNLOAD_DELAY=0.5

LOG_LEVEL='INFO'

#FEED_URI='file:///tmp/export.json'
#FEED_URI='output/export.json'
FEED_FORMAT='json'
#FEED_STORAGES=
#FEED_EXPORTERS
#FEED_STORE_EMPTY
#FEED_EXPORT_FIELDS

DOWNLOAD_TIMEOUT=60
DOWNLOAD_WARNSIZE=16777216 # 16 mb

ITEM_PIPELINES = {
    'scrashtest.pipelines.publish_date_filter.PublishDateFilterPipeline': 300
}