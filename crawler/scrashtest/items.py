import scrapy

class Page(scrapy.Item):
    url = scrapy.Field()
    keyword = scrapy.Field()
    published_at = scrapy.Field()
    excerpt = scrapy.Field()
    screenshot = scrapy.Field()
    screenshot_low = scrapy.Field()    