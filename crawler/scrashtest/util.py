# -*- coding: utf-8 -*-
import os
import errno
import hashlib
import string
import re
import time
import datetime
import scrapy
from pprint import pprint
from scrapy_splash import SplashRequest
import base64
import uuid

REGEX_YYYYMMDD = re.compile("(\d{4}\/\d{2}\/\d{2})")
REGEX_DDMMYYYY = re.compile("(\d{2}\/\d{2}\/\d{4})")
EMPTY_DATE = '3000/01/01'
EMPTY_DATE2 = '01/01/3000'


def splash_factory(url, callback, keywords):
    js_source = """
        console.log('keywords are: ' + '{k}');
        var k = "{k}".split(';');
        $('body').highlight(k);
        $(".highlight").css({ backgroundColor: "#FFFF88" });
    """
    js_source = js_source.replace('{k}', ";".join(keywords))

    return SplashRequest(
        url,
        callback,
        endpoint='render.json',
        args={
            'js': 'jquery',
            'js_source': js_source,
            'render_all': 1,
            'wait': 0.1,
            'png': 1,
            'jpeg': 1,
            'html': 1,
            'console': 1
        }
    )
    
def gen_filepath(str=None, extension=None):
    if not str or not extension:
        raise Error('missing argument: str, extension')
    
    baseDir = "/app/crawler/output/prints/" # TODO lna. dont hardcode. Create a setting?
    filepath = baseDir + hashlib.md5(str).hexdigest() + "." + extension
    
    return filepath

def write_file_basename(filepath, data):
    return os.path.basename(write_file(filepath, data))

def write_file(filepath, data):
    bytes = base64.b64decode(data)
    mkdir_p(filepath)
    with open(filepath,"wb") as f:
        f.write(bytes)
    return filepath

def mkdir_p(filepath):
    try:
        path = os.path.dirname(filepath)
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def tryFmtDateOrNone(str, fromFmt):
    try:
        return datetime.datetime.strptime(str, fromFmt).strftime('%Y/%m/%d')
    except ValueError:
        return None
