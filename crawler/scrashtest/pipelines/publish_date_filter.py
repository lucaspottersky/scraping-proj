from scrapy.exceptions import DropItem
from datetime import date, datetime, timedelta

   
def older_than(dateStr, noDays):
    d =  datetime.strptime(dateStr, '%Y/%m/%d') - timedelta(days=noDays)
    return date < d

class PublishDateFilterPipeline(object):
    NO_DAYS = 7
    
    def process_item(self, item, spider):
        if older_than(item['published_at'], self.NO_DAYS):
            spider.logger.info("Dropping item... %s - %s" % (item['url'], item['published_at']))
            raise DropItem("%s older than %s days" % (item['published_at'], self.NO_DAYS))
        
        return item
