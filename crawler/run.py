#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import os
from subprocess import call

KEYWORDS="link1;turnê;rock and ribs;rock & ribs;seu regueira;uk pub;expresso folia;the brownie factory;cabine fashion;treno fitness;bodytech;teletaxi recife;guiggo cavalcanti"

cmd_str = 'curl http://localhost:6800/schedule.json ' \
    '-d setting=LOG_LEVEL=INFO ' \
    '-d setting=DEPTH_LIMIT=2 ' \
    '-d setting=DOWNLOAD_DELAY=0.1 ' \
    '-d project=scrashtest ' \
    '-d spider=%s ' \
    '-d setting=FEED_URI=file:///app/crawler/output/%s ' \
    '--data-urlencode keywords="%s"'

# os.system("truncate --size 0 /app/crawler/output/%s" % "social1.json")
# os.system(cmd_str % ("social1", "social1.json", KEYWORDS))

# os.system("truncate --size 0 /app/crawler/output/%s" % "joao_alberto.json")
# os.system(cmd_str % ("joao_alberto", "joao_alberto.json", KEYWORDS))

# os.system("truncate --size 0 /app/crawler/output/%s" % "roberta_jungmann.json")
# os.system(cmd_str % ("roberta_jungmann", "roberta_jungmann.json", KEYWORDS))

os.system("truncate --size 0 /app/crawler/output/%s" % "local.json")
os.system(cmd_str % ("local", "local.json", KEYWORDS))
